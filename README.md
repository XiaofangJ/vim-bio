# Introduction

Bioinformatics support for vim.

This repo is a merge of following two vim plugins.

+ [ Syntax for Fasta ]( http://www.vim.org/scripts/script.php?script_id=2192 )
+ [ FASTA Manipulator ]( https://github.com/b4winckler/vim-fasta.git )

Function `FastaSingleLine` is added to allow the conversion of multiple line fasta file to single line.
