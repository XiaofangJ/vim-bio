augroup filetypedetect
    au BufNewFile,BufRead *.{fa,fasta,aa,fna}  setf fasta
    au BufNewFile,BufRead *.{gff,gtf,gff3}     setf gff
    au BufNewFile,BufRead *.{gbk,genbank}      setf genbank
    au BufRead,BufNewFile *.{nex,nexus,nxs,nx} setf nexus
augroup END
